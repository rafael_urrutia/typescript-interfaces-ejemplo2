interface ClockInterface {
    currentTime: Date;
}
 
interface ClockInterfaceMethods {
    getTime(): Date;
    getMonth(): number;
    getDay(): number;
    getHour(): number;
    getMinute(): number;
}
 
class Clock implements ClockInterface, ClockInterfaceMethods  {
    currentTime: Date;
    constructor() {
        this.currentTime = new Date();
    }
    getTime(): Date{
        return this.currentTime;
    }
 
    getMonth(): number{
        return this.currentTime.getMonth();
    }
    
    getDay(): number{
        return this.currentTime.getDay();
    }
    
    getHour(): number{
        return this.currentTime.getHours();
    }
 
    getMinute(): number{
        return this.currentTime.getMinutes();
    }
}
 
var clock = new Clock();
console.log("Fecha: " + clock.getTime());
console.log("Mes: " + clock.getMonth());
console.log("Día: " + clock.getDay());
console.log("Hora: " + clock.getHour());
console.log("Minuto: " + clock.getMinute());